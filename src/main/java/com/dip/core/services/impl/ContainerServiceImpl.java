package com.dip.core.services.impl;

import com.dip.core.models.Container;
import com.dip.core.services.ContainerService;
import javax.ejb.Stateless;

@Stateless
public class ContainerServiceImpl extends BaseServiceImpl<Container> implements ContainerService {
    public ContainerServiceImpl() {
        super(Container.class);
    }
}
