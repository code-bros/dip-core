package com.dip.core.services.impl;

import com.dip.core.exceptions.CoreException;
import com.dip.core.models.Base;
import com.dip.core.persistence.Persister;
import com.dip.core.services.BaseService;
import java.util.Collection;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

public abstract class BaseServiceImpl<T extends Base> implements BaseService<T> {

    @Inject
    protected Persister<T> persister;
    
    protected final Class<T> entityClass;
    
    public BaseServiceImpl(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public T add(T instance) throws CoreException {
        return this.persister.create(instance);
    }

    @Override
    public T get(Long id) throws CoreException {
        return this.persister.read(id);
    }

    @Override
    public Collection<T> get(Collection<Long> ids) {
        return this.persister.read(ids);
    }

    @Override
    public Collection<T> get(Map filters) {
        return this.persister.read(filters);
    }

    @Override
    public Collection<T> get() {
        return this.persister.read();
    }

    @Override
    public T update(Long id, T instance) throws CoreException {
        return this.persister.update(id, instance);
    }

    @Override
    public void delete(Long id) throws CoreException {
        this.persister.delete(id);
    }

    @PostConstruct
    private void postConstruct() {
        this.persister.setEntityClass(this.entityClass);
    }
}
