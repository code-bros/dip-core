package com.dip.core.services.impl;

import com.dip.core.models.Item;
import com.dip.core.services.ItemService;
import javax.ejb.Stateless;

@Stateless
public class ItemServiceImpl extends BaseServiceImpl<Item> implements ItemService {
    public ItemServiceImpl() {
        super(Item.class);
    }
}
