package com.dip.core.services;

import com.dip.core.models.Item;

public interface ItemService extends BaseService<Item> {

}
