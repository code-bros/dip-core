package com.dip.core.services;

import com.dip.core.exceptions.CoreException;
import com.dip.core.models.Base;
import java.util.Collection;
import java.util.Map;

public interface BaseService<T extends Base> {

    public T add(T instance) throws CoreException;

    public T get(Long id) throws CoreException;

    public Collection<T> get(Collection<Long> ids);

    public Collection<T> get(Map<String, Object> filters);
    
    public Collection<T> get();

    public T update(Long id, T instance) throws CoreException;

    public void delete(Long id) throws CoreException;
}
