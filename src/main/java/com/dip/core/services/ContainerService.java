package com.dip.core.services;

import com.dip.core.models.Container;

public interface ContainerService extends BaseService<Container> {

}
