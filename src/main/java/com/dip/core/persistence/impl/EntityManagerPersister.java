package com.dip.core.persistence.impl;

import com.dip.core.exceptions.CoreException;
import com.dip.core.exceptions.ItemNotFoundException;
import com.dip.core.models.Base;
import com.dip.core.persistence.Persister;
import java.util.*;
import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Dependent
public class EntityManagerPersister<T extends Base> implements Persister<T> {

    @PersistenceContext
    private EntityManager em;

    private Class<T> entityClass;

    @Override
    public T create(T instance) throws CoreException {
        return this.em.merge(instance);
    }

    @Override
    public T read(Long id) throws CoreException {
        return Optional.of(this.em.find(this.entityClass, id))
                .orElseThrow(() -> new ItemNotFoundException());
    }

    @Override
    public Collection<T> read(Collection<Long> ids) {
        if (!ids.isEmpty()) {
            return this.em.createQuery(
                    String.format("SELECT item FROM %s item WHERE item.id in :ids",
                    this.entityClass.getSimpleName()),
                    this.entityClass)
                    .setParameter("ids", ids)
                    .getResultList();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public Collection<T> read(Map<String, Object> filters) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(this.entityClass);
        Root root = cq.from(this.entityClass);

        filters.entrySet().stream().forEach(filter -> {
            cq.where(cb.like(root.get(filter.getKey()), "%" + filter.getValue() + "%"));
        });

        return em.createQuery(cq).getResultList();
    }

    @Override
    public Collection<T> read() {
        return this.em.createQuery(
                String.format("SELECT item FROM %s item",
                this.entityClass.getSimpleName()),
                this.entityClass).getResultList();
    }

    @Override
    public T update(Long id, T instance) throws CoreException {
        T instanceToUpdate = this.read(id);
        instanceToUpdate.merge(instance);
        return this.em.merge(instanceToUpdate);
    }

    @Override
    public void delete(Long id) throws CoreException {
        T instance = this.read(id);
        this.em.remove(instance);
    }

    @Override
    public void setEntityClass(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
}
