package com.dip.core.persistence;

import com.dip.core.exceptions.CoreException;
import com.dip.core.models.Base;
import java.util.Collection;
import java.util.Map;

public interface Persister<T extends Base> {

    public T create(T instance) throws CoreException;

    public T read(Long id) throws CoreException;

    public Collection<T> read(Collection<Long> ids);

    public Collection<T> read(Map<String, Object> filters);
    
    public Collection<T> read();

    public T update(Long id, T instance) throws CoreException;

    public void delete(Long id) throws CoreException;

    public void setEntityClass(Class<T> entityClass);
}
