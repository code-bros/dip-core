package com.dip.core.converters;

import com.dip.core.models.Value.Access;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class AccessConverter implements AttributeConverter<Access, String> {
    @Override
    public String convertToDatabaseColumn(Access attribute) {
        return attribute.name();
    }

    @Override
    public Access convertToEntityAttribute(String dbData) {
        return Access.valueOf(dbData);
    }
}
