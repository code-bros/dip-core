package com.dip.core.models;

import com.dip.core.exceptions.CoreException;
import com.dip.core.exceptions.PrototypeIncompatibilityException;
import com.dip.core.models.Value.Access;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ITEM_TYPE")
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name", "ITEM_TYPE"})
})
public abstract class Item extends Base {
    @NotNull
    protected String name;

    @ManyToOne(fetch = FetchType.EAGER)
    protected Container parent;

    @ManyToOne(fetch = FetchType.EAGER)
    protected Item prototype;

    @MapKeyJoinColumn(name = "name")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(joinColumns = @JoinColumn(name = "item_id"))
    protected Map<String, Value> properties;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Container getParent() {
        return parent;
    }

    public void setParent(Container parent) {
        this.parent = parent;
    }

    public Map<String, Value> getPropertiesModel() {



        if (this.prototype != null) {
            Map<String, Value> mergedProps = new HashMap<>();
            mergedProps.putAll(this.prototype.getPropertiesModel());
            mergedProps.putAll(this.properties);
            return mergedProps;
        } else {
            return properties;
        }
    }

    public Map<String, String> getProperties() {
        return this.getProperties(null);
    }

    /**
     * Return properties with the provided access. If access is null it returns all properties
     *
     * @param access
     * @return
     */
    public Map<String, String> getProperties(Access access) {
        return this.getPropertiesModel().entrySet().stream()
                .filter(entry -> access == null || entry.getValue().getAccess() == access)
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue().getContent()));
    }

    public void setProperties(Map<String, String> properties) {
        this.setPropertiesModel(
                properties.entrySet()
                .stream()
                .map(entry -> new AbstractMap.SimpleEntry<>(entry.getKey(), new Value(entry.getValue())))
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue())));
    }

    public void setPropertiesModel(Map<String, Value> properties) {
        this.properties = properties;
    }

    public void setProperty(String name, Value value) {
        this.properties.put(name, value);
    }

    public void setProperty(String name, String value) {
        this.setProperty(name, value, Access.PUBLIC);
    }

    public void setProperty(String name, String value, Access access) {
        this.properties.put(name, new Value(value, access));
    }

    public String getProperty(String name) {
        Value value = this.getPropertyModel(name);
        return value.getContent();
    }

    public Value getPropertyModel(String name) {
        Value value = this.properties.get(name);

        if (value != null) {
            return value;
        } else if (this.prototype != null) {
            return this.prototype.getPropertyModel(name);
        } else {
            return null;
        }
    }

    public Item getPrototype() {
        return prototype;
    }

    public void setPrototype(Item prototype) throws CoreException {
        if (this.getClass().equals(prototype.getClass())) {
            this.prototype = prototype;
        } else {
            throw new PrototypeIncompatibilityException();
        }
    }

    @Override
    public void merge(Base instance) {
        Item item = (Item) instance;
        this.name = item.getName();
        this.properties = item.getPropertiesModel();
        this.parent = item.getParent();
    }
}
