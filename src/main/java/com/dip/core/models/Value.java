package com.dip.core.models;

import com.dip.core.converters.AccessConverter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Embeddable
public class Value {
    @NotNull
    private String content;

    @Convert(converter = AccessConverter.class)
    private Access access;

    public Value() {
        this.access = Access.PUBLIC;
    }

    public Value(String content) {
        this(content, Access.PUBLIC);
    }

    public Value(String content, Access access) {
        this.content = content;
        this.access = access;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Access getAccess() {
        return access;
    }

    public void setAccess(Access access) {
        this.access = access;
    }

    public enum Access {
        PUBLIC, PRIVATE, SECURE
    }
}
