package com.dip.core.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@DiscriminatorValue("CONTAINER")
public abstract class Container extends Item {

    @OneToMany(mappedBy = "parent", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    protected Collection<Item> children;

    public Container() {
        this.children = new ArrayList<>();
    }

    public Collection<Item> getChildren() {
        return children;
    }

    public void setChildren(Collection<Item> children) {
        this.children = children;
    }

    public void addChild(Item child) {
        this.children.add(child);
    }

    @Override
    public void merge(Base instance) {
        Container container = (Container) instance;
        super.merge(instance);
        this.children = container.getChildren();
    }
}
