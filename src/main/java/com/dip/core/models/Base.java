package com.dip.core.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@MappedSuperclass
public abstract class Base implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date creationDate;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date modificationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public abstract void merge(Base instance);

    @PrePersist
    private void prePersist() {
        this.creationDate = new Date();
        this.modificationDate = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        this.modificationDate = new Date();
    }
}
