package com.dip.core.exceptions;

public class ItemNotFoundException extends CoreException {
    public ItemNotFoundException() {
        this.code = "E0001";
    }
}
