package com.dip.core.exceptions;

public abstract class CoreException extends Exception {

    protected String code;

    public CoreException(Throwable cause) {
        super(cause);
    }

    public CoreException() {}

    public String getCode() {
        return code;
    }
}
