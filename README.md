# Digital Information Platform - Core

This is the core module of the Digital Information Platform **(DIP)**

## DIP Core Architecture 

The DIP core architecture is modular one consisting of self-contained modules that perform specific role in the
whole DIP ecosystem. The bare minimum for this core system are the following modules. The core relies on CDI for any dependency injection or exposure of the models/services defined in it.

![core](https://drive.google.com/uc?id=0BxAtO_oX1AoXakk4b013R1ZITnc)

### Model

The model represents the data-structure that holds the valuable information and relationships between entities in the core.
At this point of time there are only three models in the core. It is very easy to extend the core models to add different subtypes.
The models are the primary input/output for the services. You can refer to [this](#markdown-header-dip-model) section for more details on the models.

### Service

The services offer CRUD operations for the models. In the diagram above it's depicted that the core accepts input/output in the form of Model
only. Also it can throw CoreException if something went wrong. It is very easy to do quick extensions that can laverage the generic 
service implementation in order to inherit the CRUD operations. This way the developers can spend their time building service-specific methods
that are implementing custom business logic. You can refer to [this](#markdown-header-dip-service) for more details on the services.

### Persister

The persister offers direct connection to the underlying data store. This abstraction is done so that the services can avoid vendor-locking.
By providing different types of Persister implementations, one can easily switch between different data stores, say SQL and NoSQL dbs.
You can refer to [this](#markdown-header-persister) section for more details on the persister.

### CoreException

The core is responsible to handle all exceptions in a way that the outter world can easily manage the outcome. The CoreException is the class
which should be extended to build different types of exceptions that are thrown from the core. You can refer to [this](#markdown-header-coreexception) for
more details on the exception handling and propagation.

## DIP Model

The DIP model consists of three abstract entities, and it offers core concepts found in most of the digital platforms. These models provide the base for
building specific business models by extending.

![model](https://drive.google.com/uc?id=0BxAtO_oX1AoXRjBiVDBsMDdNR0E)

### **com.dip.core.models.Base**

The Base model offers the common properties that should be shared for all the models that the business supports. This model is not to be instantiated and hence is abstract.

1. Properties
    * **id** : Long                             -- the unique id
    * **creationDate** : Date timestamp         -- the creation date of the instance
    * **modificationDate** : Date timestamp     -- the modification date of the instance

2. Lifecycle methods
    * **prePersist** : void                     -- updates the creation and modification dates for newly created and persisted instances
    * **preUpdate** : void                      -- updates the modification date for already existing but modified instances 

3. Outstanding methods
    * **merge(Base)** : void                    -- abstract and should be defined in subclasses. Merges the parameter instance with the current instace.

### **com.dip.core.models.Item extends com.dip.core.Model.Base**

The next level of abstraction is the Item model, which offers a way to describe any kind of instance in the real world. This model offers additional properties that can hold various meta data that can help to store additional details for the bunsiness
needs. Using the properties one can build various Items that represent concrete concepts. This model is abstract however, because it should be further extended
to denote more specific models that are also Items by the inheritance nature. The Item can also have a Container parent, which is described below. Most of the times
Items have some kind of a parent connection to other Items. Logically the parent can have multiple children, hence we specify it as a Container.

1. Properties
    * **name** : String                         -- simbolic name
    * **parent** : Container                    -- parent container if any, can be null
    * **properties** : Map<String, String>      -- properties map, name and content can be strings only

***Notes:***
    The properties are eagerly loaded with the instance.

### **com.dip.core.models.Container extends com.dip.core.models.Item**

Last, but not least is the Container model. In most business systems we have use-cases where we have to create different types of containers that hold children Items.
This why the core offers the Container abstraction that has children content, which is a Collection of Items. This model is also abstract because it's too generic and should
be extended into a specific Container, depending on the business needs.

1. Properties
    * **children** : Collection<Item>           -- the collection of children items that the container owns

***Notes:***
    The children collection is lazily loaded.

## DIP Service

The DIP services are operating over the models. They offer CRUD out of the box, on top of which specifics can be build.

### Persister interface

Before discussing the services, we need to understand the Persister interface. This interface is used to define the low-level connection to the data store. In this case it holds only CRUD operations.
This part of the architecture allows for the services to stay the same, while the underling data store implementation can change. The persister can be easily swapped with a different one so that we can
switch between, say NoSQL and SQL dbs.

### Persister implementation

The DIP core comes with an EntityManagerPersister. This persister works with the EntityManager from the JPA specification. This implementation is not connected to any specific SQL db. The module that uses the core
should define the `persistence.xml` and define the data source for the container to pickup while creating an EntityManager instance. As mentioned before, the core heavily relies on the DI mechanisms, specifically CDI specification.

### Service interfaces

The BaseService interface defines the minimum for a service in the DIP core. It contains the CRUD operations with some **R**ead overrides.
The ItemService and ContainerService are extensions of the BaseService and currently they do not provide any specific methods, just inherit the methods defined in the BaseService.

### Service implementations

The core offers BaseServiceImpl which is an abstract class, but defines default implementation for the CRUD using generics. This class should be extended to a more specific classes. It injects
Persister instance to actually connect to an underlying data store. The Persister is injected using CDI and is only done in runtime. This allows for easily switching between Persister implementations for specific persistence technology.
Those classes that extend BaseServiceImpl will inherit fully functional CRUD operations, and can focus on defining more business-specific methods. In the core there are the ItemServiceImpl and
ContainerServiceImpl that are specific and offer services for the Item and Container models respectively.

When defining new services, once should extend the BaseServiceImpl and continue defining the other service/business-specific methods.

![service](https://drive.google.com/uc?id=0BxAtO_oX1AoXNDNPYjdOZWUtMUk)

# Summary

This table enlists all classes and their role with short description.

| Full Qualified Class Name | Role | Type | Extends | Implements | Description |
| ------------------------- | ---- | ---- | ------- | ---------- | ----------- |
| com.dip.core.models.Base | Model | Abstract Class | N/A | N/A | The base model which every other model should extend |
| com.dip.core.models.Item | Model | Abstract Class | com.dip.core.models.Base  | N/A | Offers abstraction for items in the real world |
| com.dip.core.models.Container | Model | Abstract Class | com.dip.core.models.Item | N/A | Offers abstraction for items that are containers and have children items |
| com.dip.core.persistence.Persister | Persister | Interface | N/A | N/A | Offers persistence abstraction for flexible data store layer |
| com.dip.core.persistence.impl.EntityManagerPersister | Persister | Class | N/A | com.dip.core.persistence.Persister | Persister implementation that works with the EntityManager JPA spec and SQL db |
| com.dip.core.services.BaseService | Service | Interface | N/A | N/A | The base service interface that defines the fundamental CRUD methods |
| com.dip.core.services.ItemService | Service | Interface | com.dip.core.services.BaseService | N/A | The item service. Place to define item-specific service methods and business logic |
| com.dip.core.services.ContainerService | Service | Interface | com.dip.core.services.BaseService | N/A | The container service. Place to define container-specific service methods and business logic |
| com.dip.core.services.impl.BaseServiceImpl | Service | Abstract Class | N/A | com.dip.core.services.BaseService | The generics implementation for the Base model. It should be extended so that new services have CRUD OOB |
| com.dip.core.services.impl.ItemServiceImpl | Service | Class | com.dip.core.services.impl.BaseServiceImpl | com.dip.core.services.ItemService | The item service implementation. Even tough the Item model is abstract, this service can work with polymorfic item models |
| com.dip.core.services.impl.ContainerServiceImpl | Service | Class | com.dip.core.services.impl.BaseServiceImpl | com.dip.core.services.ContainerService | The container service implementation. Even tough the Container model is abstract, this service can work with polymorfic container models |
| com.dip.core.exceptions.CoreException | Exception | Abstract Class | Exception | N/A | The exception to wrap them all. The core should not throw anything except this or subexceptions to this one |
| com.dip.core.exceptions.ItemNotFoundException | Exception | Class | com.dip.core.exceptions.CoreException | N/A | This exception should be thrown from certain methods in the services to denote that the requested item was not found |